import 'buefy/dist/buefy.css';
import Buefy from 'buefy';
import Vue from 'vue';
import App from './App.vue';

Vue.use(Buefy, {
  defaultIconPack: "fa far fas fad fal"
});

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app');