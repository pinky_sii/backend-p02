import axios from 'axios';

const HTTP = axios.create({
  baseUrl: 'localhost:8080',
  timeout: 10000,
});
